﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhotoApplication
{
	public class Image
	{
		public DateTime AddedOn { get; set; }
		public string FullUrl { get; set; }
		public double Latitude { get; set; }
		public double Longitude { get; set; }
		public int PhotoID { get; set; }
		public string ThumbnailUrl { get; set; }
	}
}
