﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PhotoApplication
{
	public class LocalizedStrings
	{
		public LocalizedStrings() { }

		private static AppResources _localizedResources = new AppResources();

		public AppResources AppResource { get { return _localizedResources; } }
	}
}
