﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Threading;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Buddy;
using System.Windows.Controls.Primitives;
using System.ComponentModel;
using System.Diagnostics;

namespace PhotoApplication
{
    public partial class App : Application
    {
		public int selectedPhotoIndex;
		
		public Images images = new Images();
		public string auth;

        // Convenience: an app-level static accessor for BuddyClient,
        // so that it's created on demand and shared across the app.
        private static BuddyClient _buddyClient;
        public static BuddyClient BuddyClient
        {
			get
            {
				if (_buddyClient == null)
                {
					_buddyClient = new BuddyClient("PhotoApplication", "BBC8F75E-FD53-46E8-98EA-8E79C2020A7E"); // get your buddy application name and key from http://dev.buddy.com, then add a semicolon
                }   
				return _buddyClient;
            }
        }

        public static AuthenticatedUser User { get; set; }	

        public PhoneApplicationFrame RootFrame { get; private set; }

        public App()
        {
            // Global handler for uncaught exceptions. 
            UnhandledException += Application_UnhandledException;

            // Standard Silverlight initialization
            InitializeComponent();

            // Phone-specific initialization
            InitializePhoneApplication();

            // Show graphics profiling information while debugging.
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // Display the current frame rate counters.
                Application.Current.Host.Settings.EnableFrameRateCounter = true;

                PhoneApplicationService.Current.UserIdleDetectionMode = IdleDetectionMode.Disabled;
            }
        }

        // Code to execute when the application is launching (eg, from Start)
        // This code will not execute when the application is reactivated
        private void Application_Launching(object sender, LaunchingEventArgs e)
        {
			// SPLASH LOGIC
            Popup popup = new Popup();
            // Create an object of type SplashScreen.
            SplashScreen splash = new SplashScreen();
            popup.Child = splash;
            popup.IsOpen = true;
            // Create an object of type BackgroundWorker and its events.
            BackgroundWorker bw = new BackgroundWorker();
            bw.DoWork += (s, a) =>
            {
                //This event occurs while the task is executing.
                Thread.Sleep(4000); //A little dummy delay for show the effect
            };
            bw.RunWorkerCompleted += (s, a) =>
            {
                //This event occurs when the execution of the task has terminated.
                popup.IsOpen = false;
            };
            // Call to the Async Function, that produce the delay of the progress bar.
            // After that the pictured "Smoked by Windows Phone shown"
            bw.RunWorkerAsync();
        }

        // Code to execute when the application is activated (brought to foreground)
        // This code will not execute when the application is first launched
        private void Application_Activated(object sender, ActivatedEventArgs e)
        {
			// Tombstoned
			if (!e.IsApplicationInstancePreserved)
			{
				if (PhoneApplicationService.Current.State.ContainsKey("images"))
				{
					images = (Images)PhoneApplicationService.Current.State["images"];
				}
				if (PhoneApplicationService.Current.State.ContainsKey("selectedPhotoIndex"))
				{
					selectedPhotoIndex = (int)PhoneApplicationService.Current.State["selectedPhotoIndex"];
				}
				if (PhoneApplicationService.Current.State.ContainsKey("auth"))
				{
					auth = (string)PhoneApplicationService.Current.State["auth"];
				}
			}
        }

        // Code to execute when the application is deactivated (sent to background)
        // This code will not execute when the application is closing
        private void Application_Deactivated(object sender, DeactivatedEventArgs e)
        {
			// Tombstoned
			PhoneApplicationService.Current.State.Remove("images");
			PhoneApplicationService.Current.State.Add("images", images);
			PhoneApplicationService.Current.State.Remove("selectedPhotoIndex");
			PhoneApplicationService.Current.State.Add("selectedPhotoIndex", selectedPhotoIndex);
			PhoneApplicationService.Current.State.Remove("auth");
			PhoneApplicationService.Current.State.Add("auth", auth);
        }

        // Code to execute when the application is closing (eg, user hit Back)
        // This code will not execute when the application is deactivated
        private void Application_Closing(object sender, ClosingEventArgs e)
        {}

        // Code to execute if a navigation fails
        private void RootFrame_NavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // A navigation has failed; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }

        // Code to execute on Unhandled Exceptions
        private void Application_UnhandledException(object sender, ApplicationUnhandledExceptionEventArgs e)
        {
            if (System.Diagnostics.Debugger.IsAttached)
            {
                // An unhandled exception has occurred; break into the debugger
                System.Diagnostics.Debugger.Break();
            }
        }
		
        #region Phone application initialization

        // Avoid double-initialization
        private bool phoneApplicationInitialized = false;

        // Do not add any additional code to this method
        private void InitializePhoneApplication()
        {
            if (phoneApplicationInitialized)
                return;

            // Create the frame but don't set it as RootVisual yet; this allows the splash
            // screen to remain active until the application is ready to render.
            RootFrame = new PhoneApplicationFrame();
            RootFrame.Navigated += CompleteInitializePhoneApplication;

            // Handle navigation failures
            RootFrame.NavigationFailed += RootFrame_NavigationFailed;

            // Ensure we don't initialize again
            phoneApplicationInitialized = true;
        }

        // Do not add any additional code to this method
        private void CompleteInitializePhoneApplication(object sender, NavigationEventArgs e)
        {
            // Set the root visual to allow the application to render
            if (RootVisual != RootFrame)
                RootVisual = RootFrame;

            // Remove this handler since it is no longer needed
            RootFrame.Navigated -= CompleteInitializePhoneApplication;
        }
        #endregion		
    }
}