﻿using System;
using System.Windows.Controls;
using System.Windows.Media;
using Microsoft.Phone.Controls;
using System.Windows;

namespace PhotoApplication
{
    public partial class SplashScreen : UserControl
    {
        public SplashScreen()
        {
            InitializeComponent();

            // Change the background code using the same that the phone's theme (light or dark).
            this.panelSplashScreen.Background =
              new SolidColorBrush((Color)new PhoneApplicationPage().Resources["PhoneBackgroundColor"]);
            
        }

    }
}