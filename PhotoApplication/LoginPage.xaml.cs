﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using Buddy;
using System.IO.IsolatedStorage;

namespace PhotoApplication
{
    public partial class LoginPage : PhoneApplicationPage
    {
		private IsolatedStorageSettings appSettings;

		private const string passwordKey = "passwordKey";
		private const string savepasswordKey = "savepasswordKey";
		private const string usernameKey = "usernameKey";

		private string username = "";
		private string password = "";
		private bool savepassword = false;

        public LoginPage()
        {
            InitializeComponent();
			appSettings = IsolatedStorageSettings.ApplicationSettings;
        }

        public void VisibilityFunction(bool value) {
            LoginBox.IsEnabled = value;
            PasswordBox.IsEnabled = value;
            SignInButton.IsEnabled = value;
            RegisterButton.IsEnabled = value;
        }

        private void RegisterButton_Click(object sender, RoutedEventArgs e)
        {
            progressBar.Visibility = System.Windows.Visibility.Visible;
            VisibilityFunction(false);         
            App.BuddyClient.CreateUserAsync(HandleUser, LoginBox.Text, PasswordBox.Password);
        }

        private void LoginButton_Click(object sender, RoutedEventArgs e)
        {
            progressBar.Visibility = System.Windows.Visibility.Visible;
            VisibilityFunction(false);
			username = LoginBox.Text;
			password = PasswordBox.Password;
			savepassword = (bool)SavePasswordCheckBox.IsChecked;
            App.BuddyClient.LoginAsync(HandleUser, LoginBox.Text, PasswordBox.Password);
        }

        private void HandleUser(AuthenticatedUser p_user, BuddyCallbackParams p_params)
        {
            //check if everything went fine
            if (p_params.Exception != null || p_user == null)
            {
                //Display the buddy error if something went bad
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    MessageBox.Show("Something went wrong " + ((BuddyServiceException)p_params.Exception).Error);
                    progressBar.Visibility = System.Windows.Visibility.Collapsed;
                    VisibilityFunction(true);         
                });
            }
            else
            {
                //Store the user
                App.User = p_user;
                //Debug.WriteLine("Success we got our user");
                Deployment.Current.Dispatcher.BeginInvoke(() =>
                {
                    NavigationService.Navigate(new Uri("/MainPage.xaml", UriKind.Relative));
                });
            }
        }

		protected override void OnNavigatedFrom(System.Windows.Navigation.NavigationEventArgs e)
		{
			// add passwordkey and password to isolated storage
			appSettings.Remove(savepasswordKey);
			appSettings.Remove(passwordKey);

			// add username to isolated storage
			appSettings.Remove(usernameKey);
			appSettings.Add(usernameKey, username);

			if (SavePasswordCheckBox.IsChecked == true)
			{
				appSettings.Add(savepasswordKey, "true");
				appSettings.Add(passwordKey, password);
			}
			else
			{
				appSettings.Add(savepasswordKey, "false");
				appSettings.Add(passwordKey, password);
			}
		}

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
			// load password from isolated storage
			if (appSettings.Contains(passwordKey))
			{
				password = (string)appSettings[passwordKey];
			}

			// username from isolated storage
			if (appSettings.Contains(usernameKey))
			{
				username = (string)appSettings[usernameKey];
			}
			LoginBox.Text = username;

			// show password if selected to save
			if (appSettings.Contains(savepasswordKey))
			{
				string savepass = (string)appSettings[savepasswordKey];
				if (savepass == "true")
				{
					savepassword = true;
					PasswordBox.Password = password;
				}
				else
				{
					savepassword = false;
					PasswordBox.Password = "";
				}
				SavePasswordCheckBox.IsChecked = savepassword;
			}

            progressBar.Visibility = System.Windows.Visibility.Collapsed;
            VisibilityFunction(true);         

        }
    }
}