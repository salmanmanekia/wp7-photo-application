﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Tasks;
using System.Collections.ObjectModel;
using Buddy;
using System.Diagnostics;
using System.Windows.Media.Imaging;
using System.IO.IsolatedStorage;
using System.IO;
using System.Windows.Resources;
using Microsoft.Phone.Shell;

namespace PhotoApplication
{

    public partial class MainPage : PhoneApplicationPage
    {
		private App app = App.Current as App;
		private IsolatedStorageSettings appSettings;

        PhotoChooserTask photoChooserTask = new PhotoChooserTask();
		PhotoAlbum album; 

		private const string passwordKey = "passwordKey";
		private const string usernameKey = "usernameKey";
		private string username = "";
		private string password = "";

		ShellTile tile;

        public MainPage()
        {
            InitializeComponent();

			// setup our photo task, and bind the photos collection to our listbox in XAML
			photoChooserTask.ShowCamera = true;
			photoChooserTask.Completed += PhotoChooserTask_Completed;

			BuildLocalizedApplicationBar(); // application bar 
			appSettings = IsolatedStorageSettings.ApplicationSettings;

			// get application tile
			tile = ShellTile.ActiveTiles.First();
			
        }

		private void BuildLocalizedApplicationBar()
		{
			// Set the page's ApplicationBar to a new instance of ApplicationBar.
			ApplicationBar = new ApplicationBar();

			// Create a new button and set the text value to the localized string from AppResources.
			ApplicationBarIconButton appBarUploadButton =
				new ApplicationBarIconButton(new
				Uri("/Images/dark/appbar.upload.rest.png", UriKind.Relative));
			appBarUploadButton.Text = AppResources.Upload;
			appBarUploadButton.Click += new EventHandler(appBarUpload_Click);
			ApplicationBar.Buttons.Add(appBarUploadButton);
		}
		
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {			
			base.OnNavigatedTo(e);
			// load username from isolated storage
			if (appSettings.Contains(usernameKey))
			{
				string differentUsername = (string)appSettings[usernameKey];
				if (differentUsername != "") username = differentUsername;
			}		
			// load password from isolated storage
			if (appSettings.Contains(passwordKey))
			{
				password = (string)appSettings[passwordKey];
			}
			// We are coming back from ImagePage
			if (e.NavigationMode == System.Windows.Navigation.NavigationMode.Back)
			{
				PhotoList.ItemsSource = app.images;
				PhotoList.SelectedIndex = -1;
			}
			// if the album hasn't been loaded yet, load photos
			else
			{
				if (album == null)
				{				
					RefreshPhotos();
				}
			}
        }

        async void PhotoChooserTask_Completed(object sender, PhotoResult e)
        {
            if (e.ChosenPhoto != null)
            {
                // if the user has chosen a photo, add it to Buddy
                progressBar.Visibility = System.Windows.Visibility.Visible;
				var photo = await album.AddPictureAsync(e.ChosenPhoto);

				// add the photo the photo collection (which will add it to the listbox),
				// then scroll it into view
				Image image = new Image();
				image.AddedOn = photo.AddedOn;
				image.Latitude = photo.Latitude;
				image.Longitude = photo.Longitude;
				image.PhotoID = photo.PhotoID;
				image.FullUrl = photo.FullUrl;
				image.ThumbnailUrl = photo.ThumbnailUrl;
				app.images.Add(image);
				progressBar.Visibility = System.Windows.Visibility.Collapsed;
				updateTile(app.images.Count);
            }

        }

		public void updateTile(int photoCount)
		{
			if (null != tile)
			{
				// create a new data for tile
				StandardTileData data = new StandardTileData();
				// tile foreground data
				data.BackgroundImage = new Uri("/Images/FrontSide.jpg", UriKind.Relative);
				// to make tile flip add data to background also
				data.BackTitle = "Photo Application";
				data.BackBackgroundImage = new Uri("/Images/BackSide.jpg", UriKind.Relative);
				if (photoCount > 1)
				{
					data.BackContent = photoCount + " photos";
				}
				else
					data.BackContent = photoCount + " photo";
				// update tile
				tile.Update(data);
			}
		}

        private async void RefreshPhotos()
        {
            // load the current list of photos from the album
            progressBar.Visibility = System.Windows.Visibility.Visible;
            try
            {
                if (album == null)
                {
                    // first check if the album exists
                    album = await PhotoApplication.App.User.PhotoAlbums.GetAsync("My Photos");

                    if (album == null)
                    {
                        album = await PhotoApplication.App.User.PhotoAlbums.CreateAsync("My Photos");
                    }
                }
            }
            finally
            {
                progressBar.Visibility = System.Windows.Visibility.Collapsed;
            }

            // copy the values up to our observable collection, this 
            // will populate the photo listbox via data binding.
           
			app.images.Clear();
			foreach (var photo in album.Pictures)
			{
				
				Image image = new Image();
				image.AddedOn = photo.AddedOn;
				image.Latitude = photo.Latitude;
				image.Longitude = photo.Longitude;
				image.PhotoID = photo.PhotoID;
				image.FullUrl = photo.FullUrl;
				image.ThumbnailUrl = photo.ThumbnailUrl;

				app.images.Add(image);
				PhotoList.ItemsSource = app.images;
			}
			updateTile(app.images.Count);
			//bool del = await album.DeleteAsync();
        }

        private void HandleCheckin(Boolean p_result, BuddyCallbackParams p_params)
        {
            Deployment.Current.Dispatcher.BeginInvoke(() =>
            {
                if (p_result && p_params.Exception == null)
                {
                    MessageBox.Show("Success");
                }
                else
                {
                    MessageBox.Show("An error occured " + (p_params.Exception as BuddyServiceException).Error);
                }
            });
        }

		// upload click event
        private void appBarUpload_Click(object sender, EventArgs e)
        {
            photoChooserTask.Show();
        }

		// photo selection event
        private void PhotoList_SelectionChange(object sender, SelectionChangedEventArgs e)
        {
            if (PhotoList.SelectedIndex == -1)
                return;
            this.NavigationService.Navigate(new Uri("/ImagePage.xaml?photoindex=" + PhotoList.SelectedIndex, UriKind.Relative));
        }
    }
}