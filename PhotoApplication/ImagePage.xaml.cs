﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Microsoft.Phone.Controls;
using Microsoft.Phone.Shell;
using System.Windows.Media.Imaging;
using System.Diagnostics;
using Microsoft.Xna.Framework.Media;

namespace PhotoApplication
{
    public partial class ImagePage : PhoneApplicationPage
    {
		private App app = App.Current as App;

		private GestureListener gestureListener;

		private BitmapImage bitmapImage;

		// for prgress
		public bool ShowProgress
		{
			get { return (bool)GetValue(ShowProgressProperty); }
			set { SetValue(ShowProgressProperty, value); }
		}

		public static readonly DependencyProperty ShowProgressProperty =
			DependencyProperty.Register("ShowProgress", typeof(bool), typeof(ImagePage), new PropertyMetadata(false));

        public ImagePage()
        {
            InitializeComponent();
			BuildLocalizedApplicationBar();
			// for gesture
			gestureListener = GestureService.GetGestureListener(ContentPanel);
			// Handle Dragging (to show next or previous image from Album)
			gestureListener.DragCompleted += new EventHandler<DragCompletedGestureEventArgs>(gestureListener_DragCompleted);
            
        }

		private void BuildLocalizedApplicationBar()
		{
			// Set the page's ApplicationBar to a new instance of ApplicationBar.
			ApplicationBar = new ApplicationBar();

			// Create a new button and set the text value to the localized string from AppResources.
			ApplicationBarIconButton appBarDownloadButton =
				new ApplicationBarIconButton(new
				Uri("/Images/dark/appbar.download.rest.png", UriKind.Relative));
			appBarDownloadButton.Text = AppResources.Download;
			appBarDownloadButton.Click += new EventHandler(appBarDownload_Click);
			ApplicationBar.Buttons.Add(appBarDownloadButton);

		}

        protected override void OnNavigatedTo(System.Windows.Navigation.NavigationEventArgs e)
        {
			// coming from mainpage
			IDictionary<string, string> parameters = this.NavigationContext.QueryString;
			if (parameters.ContainsKey("photoindex"))
			{
				app.selectedPhotoIndex = Int32.Parse(parameters["photoindex"]);
			}
			else
			{
				app.selectedPhotoIndex = 0;
			}
            showPhoto();            
        }

		// retrieves the photo from model class and saves it as bitmap image.
		// databinds it to the image element
        private void showPhoto() {
			bitmapImage = new BitmapImage(new Uri(app.images[app.selectedPhotoIndex].FullUrl, UriKind.RelativeOrAbsolute));
			image.Source = bitmapImage;
        }

		// download. saves it in the media library
        private void appBarDownload_Click(object sender, EventArgs e)
        {
			WebClient client = new WebClient();
			client.OpenReadCompleted += (s, e1) =>
			{
				if (e1.Error == null)
				{
					MediaLibrary library = new MediaLibrary();
					String imageName = app.images.ElementAt(app.selectedPhotoIndex).PhotoID + ".jpg";
					library.SavePicture(imageName, e1.Result);
					
				}
			};
			client.OpenReadAsync(bitmapImage.UriSource);
		}

		// drag logic for the pictures. once no more images are left to be draged. It starts from the first image.
		void gestureListener_DragCompleted(object sender, DragCompletedGestureEventArgs e)
		{
			if (e.HorizontalChange > 0)
			{
				// previous image (or last if first is shown)
				app.selectedPhotoIndex--;
				if (app.selectedPhotoIndex < 0) app.selectedPhotoIndex = app.images.Count - 1;
			}
			else
			{
				// next image (or first if last is shown)
				app.selectedPhotoIndex++;
				if (app.selectedPhotoIndex > (app.images.Count - 1)) app.selectedPhotoIndex = 0;
			}
			showPhoto();
		}

    }
}